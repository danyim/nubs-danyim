'use strict';

angular.module('danyimApp')
  .controller('NavbarCtrl', ['$scope', '$location', function ($scope, $location) {
    // $scope.date = new Date();

    // Neat idea from http://stackoverflow.com/questions/12592472/how-to-highlight-a-current-menu-item
    $scope.isActive = function (viewLocation) {
     var active = (viewLocation === $location.path());
     return active;
    };
  }]);
