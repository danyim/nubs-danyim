'use strict';

angular.module('danyimApp')
  .controller('AboutCtrl', function ($scope, staticSvc) {
    $scope.makeCopies = staticSvc.makeCopies;
  });
