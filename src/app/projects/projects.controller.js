'use strict';

angular.module('danyimApp')
  .controller('ProjectsCtrl', function ($scope, staticSvc) {
    $scope.makeCopies = staticSvc.makeCopies;
  });
