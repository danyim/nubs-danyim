<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title>Daniel Yim - Web Developer</title>
    <meta name="title" content="Daniel D. Yim">
    <meta name="keywords" content="Daniel Yim, danyim, nubs.org, Daniel D Yim, daniel yim houston">
    <meta name="description" content="Daniel D. Yim's personal website containing projects, professional information, and a small summary.">
    <link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="_/css/app.css">
</head>
<body>
<div class="container">
    <div id="header" class="row">
        <ul class="list-unstyled list-inline profileLinks">
            <li>
                <a href="https://www.linkedin.com/in/danielyim" target="_blank" title="View LinkedIn profile">
                    <i class="fa fa-linkedin"></i>
                </a>
            </li>
            <li>
                <a href="https://github.com/danyim" target="_blank" title="View GitHub profile">
                    <i class="fa fa-github"></i>
                </a>
            </li>
            <li>
                <a href="https://bitbucket.org/danyim" target="_blank" title="View BitBucket profile">
                    <i class="fa fa-bitbucket"></i>
                </a>
            </li>
            <li>
                <a href="http://stackoverflow.com/users/350951/danyim" target="_blank" title="View StackOverflow profile">
                    <i class="fa fa-stack-overflow"></i>
                </a>
            </li>
        </ul>
        <div class="myName">
            <span class="first">DANIEL</span>
            <span class="last">YIM</span>
        </div>
        <div id="navi" class="">
            <ul class="list-unstyled list-inline">
                <li>
                    <a href="#about">
                        <div class="element">
                            <span class="sym">Ab</span><br />
                            <span class="title">About</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#portfolio">
                        <div class="element">
                            <span class="sym">Pf</span><br />
                            <span class="title">Portfolio</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#projects">
                        <div class="element">
                            <span class="sym">Pr</span><br />
                            <span class="title">Projects</span>
                        </div>
                    </a>
                </li>
            </ul>
        </div>

    </div>
    <div id="content" class="row">
        <div class="">
            <!-- MAIN -->
            <p>Hi there.</p>
            <p>
                This is a small corner of the web that I reserved for all things related to <em>me</em>, the Daniel Yim from Houston, as an attempt to distinguish myself from the others who share this awesome name. Although I'm certain it's a common Korean-American moniker, I'd like to think that I am the only computer scientist and developer among them. I'm probably dead wrong, but I would say that I'm far enough ahead of the others since I snatched up the danielyim/danyim aliases on most popular websites. Go me!
            </p>
            <p>
                A little history about this site: <a href="http://nubs.org" target="_blank">nubs.org</a> (est. 2003) was the very first domain that I had purchased. In its hayday, it hosted a variety of content through its subdomains, but the website's primary purpose has always been to be an environment where I can experiment with the capabilities of the web.
            </p>

            <!-- ABOUT -->
            <div class="section-header"><a name="about">about me</a></div>
            <p>
                After graduating from Stephen F. Austin State University with a degree in Computer Science, I headed straight into the industry and learned the ropes at <a href="http://www.valic.com" target="_blank">AIG VALIC</a> as an ASP.NET developer. My curiosity to explore technologies outside of the Microsoft realm eventually led me to accept a position as a consultant at <a href="http://www.parivedasolutions.com" target="_blank">Parvieda Solutions</a>.
            </p>
            <p>
                My interests are spread across many different disciplines, but right now I am particularly interested in modern web design, augmented reality, and wearable tech.
            </p>
            <p>
                When I'm not being a total nerd, you may find me running at events (5Ks, half and full marathons), out in the Texas country riding my road bike, or playing the blues on my bedroom rockstar guitar rig.
            </p>
            <p>
                You may contact me directly at <a href="mailto:danielyim@gmail.com" target="_blank">danielyim@gmail.com</a>.
            </p>

            <!-- PORTFOLIO -->
            <div class="section-header"><a name="portfolio">portfolio</a></div>
            <p>
                At the moment, I am open to any opportunities where I can capitalize on my programming, designing, and teamwork abilities.
            </p>
            <p>
                An informal version of my resume can be found on my <a href="http://www.linkedin.com/in/danielyim" target="_blank">LinkedIn profile</a>. Feel free to drop me a line via email if you would like to see my full resume.
            </p>

            <div class="featurette">
                <img class="featurette-image pull-right" src="_/img/mach_huge.png">
                <h2 class="featurette-heading">MACH INTERVIEW</h2>
                <p class="lead">Picked up a botched project from a contractor and redesigned the entire website's backend. Uses a LAMP stack with jQuery supporting the front-end. Entire project converted to use an object-oriented model.</p>
            </div>

            <!-- <hr class="featurette-divider">

            <div class="featurette">
                <img class="featurette-image pull-left" src="_/img/mach_huge.png">
                <h2 class="featurette-heading">FUND PERFORMANCE</h2>
                <p class="lead">Designed a publicly displayable minisite off of design masters in ASP.NET.</p>
            </div> -->

            <!-- PROJECTS -->
            <div class="section-header"><a name="projects">projects</a></div>
            <p>
                I am always looking to expand my horizons as a developer, so aside from the work that I do for my nine-to-five gig, I work on some side projects to keep it interesting.
            </p>

            <div class="subsection-header">iOS Development &nbsp;&nbsp;<i class="fa fa-apple"></i></div>
            <ul>
                <li>
                    <a href="http://online.stanford.edu/course/developing-ios7-apps-fall-2013">CS 193P iPhone Application Development iTunesU Course</a>
                    <p>
                        Taking the initiative to enter the realm of iOS development. I started this course in March 2014 and will be posting the solutions to the homework assignments to this <a href="https://bitbucket.org/danyim/cs-193p-solutions" target="_blank">BitBucket repo</a>.
                    </p>
                </li>
            </ul>
            <!-- <div class="subsection-header">Android Development &nbsp;&nbsp;<i class="fa fa-android"></i></div>
            <ul>
                <li>
                    Some project
                    <p>
                    </p>
                </li>
            </ul> -->

            <div class="subsection-header">web design</div>
            <ul>
                <!-- <li>
                    <a href="http://www.gulfcoastexoticbirdsanctuary.com/" target="_blank">Gulf Coast Exotic Bird Sanctuary</a>
                    <p>
                        I volunteer my free time to this wonderful non-profit organization that provides a safe environment for birds and other exotic animals in the Houston area.
                    </p>
                </li> -->
                <li>
                    <a href="http://www.nubs.org/danyim/">Personal Website</a>
                    (May 2014)
                    <p>
                        I used the redesign of this website as a medium for adopting the workflow <a href="https://www.youtube.com/watch?v=vsTrAfJFLXI">as presented in this talk by Chris Coyier</a> for all of my web development going forward. Before I took on this project, my web projects were conducted on Windows using nothing but Notepad++. Oh, how could I have been so <em>basic</em>?
                    </p>
                    <p>
                        Through this experience, I transitioned all my work to the MacBook, purchased the necessary software (Sublime Text, CodeKit, MAMP), and started getting my feet wet with the neat tools that the modern web offers (SCSS, Bootstrap, <a href="http://kickoff.thehivemedia.com/">CodeKit Kickoff scaffolding</a>). In the end, I can't believe how long I have stayed content with my primitive web dev workflow.
                    </p>
                </li>
            </ul>
            <div class="subsection-header">miscellaneous</div>
            <ul>
                <li>
                    <a href="http://projecteuler.net/profile/danyim.png" target="_blank">Project Euler</a>
                    <p>
                        My goal is to work though most (if not all) of these mathematically inclined programming problems strictly in Python in order to help me learn the language. I am currently working on adding my solutions to a public repo on my <a href="https://github.com/danyim" target="_blank">GitHub account</a>.
                    </p>
                </li>
                <li>
                    <a href="http://stackoverflow.com/users/350951/danyim" target="_blank">Stack Overflow</a>
                    <!-- <p>
                        My SO account isn't necessarily packed with insightful questions or exceptional answers, but I aim to contribute to the community when I can.
                    </p> -->
                </li>
                <li>
                    <a href="http://topcoder.com/">TopCoder</a>
                </li>
            </ul>
            <div class="subsection-header">open source</div>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </div>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </div>

    <div id="footer" class="row">
        <div class="">
            <span class="date">&copy; 2014 Daniel Yim. Built with <a href="https://incident57.com/codekit/" target="_blank">CodeKit</a>, <a href="http://getbootstrap.com/" target="_blank">Bootstrap</a>, and TLC on a MacBook Pro.</span>
            <br /><br />
        </div>
    </div>
</div>
<script type="text/javascript" src="_/js/app.js"></script>
</body>
</html>